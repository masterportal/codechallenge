import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AdminIcon from '@material-ui/icons/Person';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import compose from 'recompose/compose';
import {
	loadAllApplications,
	filterAndSortingApplications,
} from '../../store/actions';
import ApplicationList from '../../Components/Table';
import queryString from 'query-string';

/*These styles is used as classes in Home class */
const styles = theme => ({
	content: {
		backgroundColor: theme.palette.background.default,
		padding: theme.spacing.unit * 3,
		paddingTop: 0,
		height: '100%',
	},
	progress: {
		margin: theme.spacing.unit * 20,
		marginLeft: '36vw',
		color: 'red',
	},
});

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			applications: [],
		};
	}
	/*
	 *Updated the state of applications when it normally gets as props
	 */

	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.applications && nextProps.applications.length > 0) {
			return {
				applications: nextProps.applications,
			};
		}
		// Return null to indicate no change to state.
		return null;
	}
	componentDidMount() {
		const { location } = this.props;
		const parsed = queryString.parse(location.search);
		/*
		 *If user refreshes the url with parameters and it determines if users passed the parameters or not
		 */
		if (parsed && Object.keys(parsed).length > 0) {
			this.props.filterAndSortingApplications(parsed);
		} else {
			this.props.loadAllApplications();
		}
	}

	changeUrl = genericObject => {
		const { history } = this.props;
		const { filter, sort } = genericObject;
		/*
		 *Building the query string here and then it pushes the history
		 *It will not refresh the page unless the user do so by himself/herself
		 */
		let queryString = '?';
		if (genericObject.filter.name !== '') {
			queryString = queryString + `name=${genericObject.filter.name}`;
		}
		if (genericObject.filter.position !== '') {
			queryString = queryString !== '?' ? queryString + '&&' : '?';
			queryString =
				queryString + `position=${genericObject.filter.position}`;
		}
		if (filter.select !== '' && filter.select.toLowerCase() !== 'none') {
			queryString = queryString !== '?' ? queryString + '&&' : '?';
			queryString = queryString + `select=${genericObject.filter.select}`;
		}
		if (sort.fieldName !== '') {
			queryString = queryString !== '?' ? queryString + '&&' : '?';
			queryString =
				queryString + `sort=${sort.fieldName}&&order=${sort.order}`;
		}
		if (queryString !== '?') {
			history.push({
				pathname: '/',
				search: queryString,
			});
		}
	};

	render() {
		const { classes } = this.props;
		const { applications } = this.state;

		return (
			<div>
				<main className={classes.content}>
					<ListItem>
						<ListItemIcon>
							<AdminIcon className={classes.profileIcon} />
						</ListItemIcon>
						<ListItemText inset primary="Applications" />
					</ListItem>
					{/*Change url is always called by child component and it will update the url with query string */}
					{/*If there would be no state in our application it will show the loader
					 *we could determine by using isloaded boolean variable as redux state but i assumed there would
					 *be always data
					 */}
					{applications && applications.length > 0 ? (
						<ApplicationList
							changeUrl={genericObject =>
								this.changeUrl(genericObject)
							}
							applications={applications}
						/>
					) : (
						<CircularProgress
							className={classes.progress}
							thickness={2}
						/>
					)}
				</main>
			</div>
		);
	}
}

Home.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired,
	applications: PropTypes.array.isRequired,
};

//map dispatch action methods to props

const mapDispatchToProps = dispatch => {
	return {
		loadAllApplications: () => {
			dispatch(loadAllApplications());
		},
		filterAndSortingApplications: genericObject => {
			dispatch(filterAndSortingApplications(genericObject));
		},
	};
};

//map redux state to props

const mapStateToProps = state => {
	return {
		applications: state.applications,
	};
};

export default compose(
	withStyles(styles, { withTheme: true }),
	connect(
		mapStateToProps,
		mapDispatchToProps
	)
)(Home);
