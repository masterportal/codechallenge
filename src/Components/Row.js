import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import moment from 'moment';

//Calculate the age from the current date using the user date of birth
const calculateAge = birthdate => {
	return moment().diff(moment(birthdate, 'YYYYMMDD'), 'years');
};

const styles = theme => ({
	tableCell: {
		paddingRight: 4,
	},
	textAlign: {
		textAlign: 'center',
	},
});

/**
 * Creating the row component
 * It renders as each table row
 * application is placed as props
 */

const Row = ({ application, classes }) => (
	<TableRow key={application.id}>
		<TableCell>{application.name}</TableCell>
		<TableCell>{application.email}</TableCell>
		<TableCell numeric className={classes.textAlign}>
			{calculateAge(application.birth_date)}
		</TableCell>
		{/* For numeric column , made text alignment centered */}
		<TableCell numeric className={classes.textAlign}>
			{application.year_of_experience}
		</TableCell>
		<TableCell>{application.position_applied}</TableCell>
		<TableCell>
			{/*Format the application date using moment */}
			{moment(application.application_date).format('DD/MM/YYYY')}
		</TableCell>
		<TableCell>{application.status}</TableCell>
	</TableRow>
);

Row.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Row);
