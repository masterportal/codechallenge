import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import classNames from 'classnames';
import { filterApplications } from '../store/actions';
import Row from './Row';

const styles = theme => ({
	root: {
		display: 'flex',
		marginTop: theme.spacing.unit * 3,
		overflow: 'scroll',
		maxWidth: '100%',
	},
	table: {
		display: 'block',
		width: '100%',
		overflowX: 'auto',
	},

	tableCell: {
		paddingRight: 4,
	},
	textAlign: {
		textAlign: 'center',
	},
	selectList: {
		backgroundColor: '#fff',
	},
	inputText: {
		border: '1px solid rgba(0,0,0,0.1)',
		padding: '5px 7px',
		outline: 'none',
		marginTop: '5px',
	},
	icon: {
		fontSize: '15px',
	},
	cursor: {
		cursor: 'pointer',
	},
});

class ApplicationList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			filter: {
				name: '',
				position: '',
				select: '',
			},
			sort: {
				fieldName: '',
				order: 'desc',
			},
		};
	}
	/*
	 *updates the filter value of name
	 */
	handleChangeName = event => {
		//spread operator to make the state immutable
		let filterObj = { ...this.state.filter };
		filterObj.name = event.target.value;
		this.setState({ filter: filterObj }, () => {
			//updates the state based on filter value of name
			this.props.filterAndSortApplications(this.state);
			//updates the url and passed value of name as parameters but it will not refresh the page
			this.props.changeUrl(this.state);
		});
	};
	/*
	 *updates the filter value of position applied
	 */
	handleChangePosition = event => {
		let filterObj = { ...this.state.filter };
		filterObj.position = event.target.value;
		this.setState({ filter: filterObj }, () => {
			this.props.filterAndSortApplications(this.state);
			this.props.changeUrl(this.state);
		});
	};
	/*
	 *update the filter value of select choice like approved, waiting and rejected
	 */
	handleSelectChange = event => {
		let filterObj = { ...this.state.filter };
		filterObj.select = event.target.value;
		this.setState({ filter: filterObj }, () => {
			this.props.filterAndSortApplications(this.state);
			this.props.changeUrl(this.state);
		});
	};
	/*
	 *update the sorting value of field and first time it will sorts the column as descending order
	 */
	handleSort = fieldName => {
		let sortObj = { ...this.state.sort };
		if (fieldName === sortObj.fieldName) {
			sortObj.order = sortObj.order === 'asc' ? 'desc' : 'asc';
		} else {
			sortObj.order = 'desc';
		}
		sortObj.fieldName = fieldName;
		this.setState({ sort: sortObj }, () => {
			this.props.filterAndSortApplications(this.state);
			this.props.changeUrl(this.state);
		});
	};
	render() {
		const { applications, classes } = this.props;
		const { filter, sort } = this.state;
		// it filters the applications based on the values
		const SelectChoice = () => (
			<div>
				<Select
					className={classes.selectList}
					value={filter.select}
					onChange={this.handleSelectChange}
					inputProps={{
						name: 'filter',
						id: 'filter-by',
					}}
				>
					<MenuItem value={'none'}>None</MenuItem>
					<MenuItem value={'approved'}>Approved</MenuItem>
					<MenuItem value={'rejected'}>Rejected</MenuItem>
					<MenuItem value={'waiting'}>Waiting</MenuItem>
				</Select>
			</div>
		);
		return (
			<Paper className={classes.root}>
				<Table className={classes.table}>
					<TableHead>
						<TableRow>
							<TableCell className={classes.tableCell}>
								Name{' '}
								<div>
									<input
										type="text"
										className={classes.inputText}
										onChange={this.handleChangeName}
										value={this.state.name}
									/>
								</div>
							</TableCell>
							<TableCell>Email</TableCell>
							<TableCell numeric className={classes.textAlign}>
								Age
							</TableCell>
							<TableCell
								numeric
								className={classNames(
									classes.textAlign,
									classes.cursor
								)}
								onClick={() =>
									this.handleSort('year_of_experience')
								}
							>
								Years of Experience{' '}
								<span>
									{sort.fieldName === 'year_of_experience' ? (
										sort.order === 'asc' ? (
											<ArrowUpward
												className={classes.icon}
											/>
										) : (
											<ArrowDownward
												className={classes.icon}
											/>
										)
									) : null}
								</span>
							</TableCell>
							<TableCell className={classes.cursor}>
								{/* Used span to distinguish between sort and filter */}
								<span
									onClick={() =>
										this.handleSort('position_applied')
									}
								>
									Position applied{' '}
								</span>
								{/*It displays the arrow symbol at the right side of each field
								 *and will show the arrow symbol of current field only
								 *it displays the upward arrow or downward depends upon its current state
								 */}
								<span>
									{sort.fieldName === 'position_applied' ? (
										sort.order === 'asc' ? (
											<ArrowUpward
												className={classes.icon}
											/>
										) : (
											<ArrowDownward
												className={classes.icon}
											/>
										)
									) : null}
								</span>
								<div>
									<input
										type="text"
										className={classes.inputText}
										onChange={this.handleChangePosition}
										value={this.state.position}
									/>
								</div>
							</TableCell>
							<TableCell
								className={classes.cursor}
								onClick={() =>
									this.handleSort('application_date')
								}
							>
								Applied{' '}
								<span>
									{sort.fieldName === 'application_date' ? (
										sort.order === 'asc' ? (
											<ArrowUpward
												className={classes.icon}
											/>
										) : (
											<ArrowDownward
												className={classes.icon}
											/>
										)
									) : null}
								</span>
							</TableCell>
							<TableCell>
								Status <SelectChoice />
							</TableCell>
						</TableRow>
					</TableHead>

					<TableBody>
						{applications && applications.length > 0
							? applications.map((application, i) => (
									<Row
										application={application}
										key={application.id}
									/>
							  ))
							: null}
					</TableBody>
				</Table>
			</Paper>
		);
	}
}

ApplicationList.propTypes = {
	classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => {
	return {
		filterAndSortApplications: genericObj => {
			dispatch(filterApplications(genericObj));
		},
	};
};

export default compose(
	withStyles(styles, { withTheme: true }),
	connect(
		null,
		mapDispatchToProps
	)
)(ApplicationList);
