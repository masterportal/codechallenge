import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import './App.css';
import createBrowserHistory from 'history/createBrowserHistory';
import routes from './routes.js';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { createMuiTheme } from '@material-ui/core/styles';
import './styles/main.scss';

/* create redux store for storing client data */
const store = configureStore();

/**
 * Creates a history object that uses the HTML5 history API including
 * pushState, replaceState, and the popstate event.
 */
const history = createBrowserHistory();

const theme = createMuiTheme();

class App extends Component {
	render() {
		return (
			<MuiThemeProvider theme={theme}>
				<Provider store={store}>
					<Router history={history}>{routes}</Router>
				</Provider>
			</MuiThemeProvider>
		);
	}
}

export default App;
