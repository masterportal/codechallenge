import React from 'react';
import { Switch, Route } from 'react-router';
import Home from '../src/Containers/Home/Home';

export default (
	<div>
		<Switch>
			<Route path="/" component={Home} />
		</Switch>
	</div>
);
