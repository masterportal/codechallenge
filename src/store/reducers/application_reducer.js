import { LOAD_APPLICATIONS } from '../actions/actionTypes';

export default function(state = [], action) {
	switch (action.type) {
		case LOAD_APPLICATIONS:
			state = [];
			//creating an immutable array and adding the applications array to store
			return [...state, ...action.payload];
		default:
			return state;
	}
}
