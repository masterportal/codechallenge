import { LOAD_APPLICATIONS, LOAD_ORIGINAL_APPLICATIONS } from './actionTypes';
import axios from 'axios';
import _ from 'lodash';

/*
 * Load applications state when no url is passed as parameters
 */
export const loadAllApplications = () => {
	return dispatch => {
		axios
			.get('http://personio-fe-test.herokuapp.com/api/v1/candidates')
			.then(response => {
				dispatch({
					type: LOAD_APPLICATIONS,
					payload: response.data.data,
				});
				dispatch({
					type: LOAD_ORIGINAL_APPLICATIONS,
					payload: response.data.data,
				});
			})
			.catch(err => console.log(err));
	};
};

/*
 *Filter the applications state by using  key as an attribute of application
 * and user value entered as value
 */
const filterByValue = (applications, key, value) => {
	return applications.filter(application => {
		return (
			application[key].toLowerCase().indexOf(value.toLowerCase()) === 0
		);
	});
};

/*
 *sort the applications state and here order can be ascedning as asc
 *and descending as desc
 */

const sortByApplications = (applications, fieldName, order) => {
	if (fieldName !== '') {
		let result = _.orderBy(applications, [fieldName], [order]);
		return result;
	}
	return applications;
};

/*
 *This function is normally called while filtering and sorting the table
 *It's the generic function and called also while passing parameters in url
 */

export const filterApplications = (genericObj, origApplications) => {
	return (dispatch, getState) => {
		const { filter, sort } = genericObj;
		let originalApplications = [];
		// if applications are passed as parameters when passed the parameters in url
		if (origApplications && origApplications.length > 0) {
			originalApplications = origApplications;
		} else {
			originalApplications = getState().originalApplications;
		}
		//check the sort each time while filtering
		let applications = sortByApplications(
			originalApplications,
			sort.fieldName,
			sort.order
		);
		let filteredApplications = [];

		//filter by name
		if (filter.name !== '') {
			filteredApplications = filterByValue(
				applications,
				'name',
				filter.name
			);
		} else {
			filteredApplications = applications;
		}
		//filter again the filtered array by position
		if (filter.position !== '') {
			filteredApplications = filterByValue(
				filteredApplications,
				'position_applied',
				filter.position
			);
		}
		/*
		 *Here solved the specific case if someone passes 'a' then filters the applications
		 *using the value as first letter
		 */
		if (filter.select !== '' && filter.select.toLowerCase() !== 'none') {
			filteredApplications = filteredApplications.filter(application => {
				return (
					application.status.toLowerCase() ===
					filter.select.toLowerCase()
				);
			});
		}
		/*
		&Updated the application state by passing the filtered applications
		*/
		dispatch({
			type: LOAD_APPLICATIONS,
			payload: filteredApplications,
		});
	};
};

/*
 *Always call when we refresh the application with url parameters
 */

export const filterAndSortingApplications = genericObj => {
	return dispatch => {
		//creating the new generic object
		let newObj = {
			filter: {
				name: genericObj.name ? genericObj.name : '',
				position: genericObj.position ? genericObj.position : '',
				select: genericObj.select ? genericObj.select : '',
			},
			sort: {
				fieldName: genericObj.sort ? genericObj.sort : '',
				order: genericObj.order ? genericObj.order : '',
			},
		};
		axios
			.get('http://personio-fe-test.herokuapp.com/api/v1/candidates')
			.then(response => {
				/*
				 *i maintained the two stores one for original applications which are intial ones
				 *other one is the current state of applications
				 */
				dispatch({
					type: LOAD_ORIGINAL_APPLICATIONS,
					payload: response.data.data,
				});
				dispatch(filterApplications(newObj, response.data.data));
			})
			.catch(err => console.log(err));
	};
};
