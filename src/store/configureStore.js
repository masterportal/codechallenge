import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import applicationsReducer from './reducers/application_reducer';
import applicationOriginalReducer from './reducers/application_original_reducer';

//combine all reducers
const rootReducer = combineReducers({
	applications: applicationsReducer,
	originalApplications: applicationOriginalReducer,
});

let composeEnhancers = compose;

if (process.env.NODE_ENV === 'development') {
	composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const configureStore = () => {
	return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
};

export default configureStore;
